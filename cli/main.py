#!/usr/bin/env python3

import cmd
import os
import socket
import subprocess
import netifaces
import re


class GetSysInfo:

    def __init__(self):
        self.__nicInfo = {'ip': [], 'netmask': []}
        self.__libList = ['netifaces', 'argparse', 'ipaddress', 'socket', 'accessify', 'netifaces']
        self.__hostInfo = [ 'hostname']
        self.__defGateway = {'ifname_name': '', 'ip': ''}

        try:
            self._hostname = socket.gethostname() + '> '
        except socket.error:
            self._hostname = '# '

    def is_valid_hostname(self, _hostname):
        if len(_hostname) > 255:
            return False
        if _hostname[-1] == ".":
            _hostname = _hostname[:-1]  # strip exactly one dot from the right, if present
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        return all(allowed.match(x) for x in _hostname.split("."))

    def get_host_name(self):
        return self._hostname

    def get_default_gw(self):
        gws = netifaces.gateways()
        if gws:
            self.__defGateway['iface_name'] = gws['default'][netifaces.AF_INET][1]
            self.__defGateway['ip']         = gws['default'][netifaces.AF_INET][0]
        else:
            print("No default gateway set.")

    def get_nic_settings(self):
        if self.__defGateway['iface_name']:
            addrs = netifaces.ifaddresses(self.__defGateway['iface_name'])
            if addrs:
                self.__nicInfo['ip']        = addrs[netifaces.AF_INET][0]['addr']
                self.__nicInfo['netmask']   = addrs[netifaces.AF_INET][0]['netmask']
            else:
                print("No ip address set.")
        else:
            print("No default gateway set.")

    def print_nic_settings(self):
        self.get_default_gw()
        self.get_nic_settings()
        if self.__nicInfo['ip']:
            print("IP address: %s\nNetwork mask: %s\nDefault gateway: %s" % (self.__nicInfo['ip'], self.__nicInfo['netmask'], self.__defGateway['ip']))


    def set_host_name(self, _host_name):
        # Set hostname;
        subprocess.run(['sudo', 'hostname', _host_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        subprocess.call('echo {} | sudo tee /etc/hostname'.format(_host_name),
                        shell=True,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE)
        self._hostname = _host_name + "# "


class AppCLI(cmd.Cmd):
    systemInfo = GetSysInfo()
    intro = 'Welcome to SmartApp!!! Type ? to list commands.'
    prompt = systemInfo.get_host_name()
    _COMMANDS = ['directory', 'files']

    def refresh_prompt_name(self):
        self.prompt = self.systemInfo.get_host_name()

    def do_greet(self, person):
        "Greet the person"
        if person and person in self._COMMANDS:
            greeting = 'hi, %s!' % person
        elif person:
            greeting = "hello, " + person
        else:
            greeting = 'hello'
        print(greeting)

    def complete_greet(self, text, line, begidx, endidx):
        if not text:
            completions = self._COMMANDS[:]
        else:
            completions = [f
                           for f in self._COMMANDS
                           if f.startswith(text)
                           ]
        return completions

    def do_show(self, param):
        input_params = param.split()
        if (param == 'dir'):
            print('dir')
        elif (param == 'pwd'):
            print(os.path.dirname(os.path.abspath(__file__)))
        elif (param == 'ifconfig'):
            self.systemInfo.print_nic_settings()
        # Print hostname;
        elif (input_params[0] == 'hostname'):
            print('Hostname')

    # Set command implementation;
    def do_set(self, param):
        input_params = param.split()
        if len(input_params) == 2:
            if (input_params[0] == 'hostname') and self.systemInfo.is_valid_hostname(input_params[1]):
                self.systemInfo.set_host_name(input_params[1])
                self.refresh_prompt_name()
            else:
                print("Please input right argument please.")

    def _check_parameters(self, _params, _len, _type):
        if len(_params == _len):
            return True
        else:
            return False

    def do_exit(self, inp):
        print("Good Buy!!!")
        return True

if __name__ == '__main__':
    AppCLI().cmdloop()
